# storm-demo

Demo project to show a way of integrating Apache Kafka, Apache Storm and Google Guice.
The topology consumes and produces avro messages and executes calculations on the received data.
